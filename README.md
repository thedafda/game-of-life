# Game Of Life #

[ ![Codeship Status for thedafda/game-of-life](https://codeship.com/projects/74b25510-08a4-0133-b978-52c6dae51101/status?branch=master)](https://codeship.com/projects/90366)

Production Link: https://s3-ap-southeast-1.amazonaws.com/game.of.life/index.html

-----------------

##Technology Stack##

* Javascript, HTML/CSS
* jQuery
* Lodash
* Angular
* Jasmine for unit test with Karma test runner
* Grunt

------------------

##Directory Structure##

* ./public/* - public facing application code
* ./public/scripts/* - all javascript files
* ./public/styles/* - all styles files
* ./dist/* - production(minified) output directory 
* ./tests/* - all unit tests

------------------

##Required Softwares##

* NodeJS and NPM

* Grunt CLI ( can be downloaded using `npm install -g grunt-cli` command)

------------

##Commands##

Before executing any of the below commands once run `npm install` in project directory. this will download the required dependencies to the project.
  
* To start static development server use `grunt connect:dev:keepalive`. and go to http://localhost:9001
* To build production version - use `grunt build`. this will create minified and production ready version of whole project in `dist` directory.  
* To run unit tests -use `grunt unit-test`.  this will run unit test in chrome, phantom and firefox.
* To run jshint - use `grunt jshint`.