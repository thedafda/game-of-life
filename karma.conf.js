module.exports = function(config) {
  config.set({
    basePath: '.',
    frameworks: ['jasmine'],
    files: [
      // LIbs
      "public/scripts/lib/lodash.min.js",
      "public/scripts/lib/jquery.min.js",
      "public/scripts/lib/angular.min.js",
      // Test Lib
      "test-lib/angular-mocks.js",
      // Core
      "public/scripts/core/game_of_life.js",
      // Wrappers
      "public/scripts/jquery_components/jq_game_of_life.js",
      "public/scripts/angular_directives/ng_game_of_life.js",
      // Tests
      'tests/**/*.js'
    ],
    reporters: ['mocha'],
    browsers: ['PhantomJS', 'Chrome', 'Firefox'], //PhantomJS,Chrome,ChromeCanary
    singleRun: true
  });
};