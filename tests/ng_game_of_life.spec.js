describe('Angular wrapper', function(){

	beforeAll(function(){
		this.getElementInGrid = function(grid, row, col){
			return grid.children[row].children[col];
		};
	});

	describe("#GameOfLife: Angular", function(){

		beforeAll(function(){
			this.template = '<game-of-life cols="cols" rows="rows" options="options"></game-of-life>';
		});

		beforeAll(function(){
			angular.module('MockedModule', ['GameOfLife']);
		});

		beforeEach(module('MockedModule'));

		beforeEach(inject(function($rootScope, $compile) {
			this.$rootScope = $rootScope;
			this.$compile = $compile;
			this.testContainer = document.createElement('div');
			document.body.appendChild(this.testContainer);
			this.compileDirective = function(template, scope) {
			  var element = this.$compile(template)(scope);
			  this.testContainer.appendChild(element[0]);
			  scope.$digest();
			  return element;
			};
		}));

		afterEach(function(){
			document.body.removeChild(this.testContainer);
		});


		it('angular: should generate grid of 10 X 10', function(){
			var $scope = this.$rootScope.$new();
			$scope.rows = 10;
			$scope.cols = 10;
			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			expect(tbody.children.length).toEqual(10);
			$.each(tbody.children, function(index, row){
				expect(row.children.length).toEqual(10);
			});
		});

		it('angular: should not have any alive cell by default', function(){
			var $scope = this.$rootScope.$new();
			$scope.rows = 10;
			$scope.cols = 10;
			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			$.each(tbody.children, function(rindex, row){
				$.each(row.children, function(cindex, col){
					expect($(col).hasClass('dead')).toBeTruthy();
					expect($(col).hasClass('alive')).toBeFalsy();
				});
			});
		});

		it('angular: should set alive to initialValues list', function(){
			var scope = this;
			var arrList = [
				[0, 1],[5, 4],
				[3, 4],[4, 9],
				[5, 6],[6, 6]
			];

			var $scope = this.$rootScope.$new();
			$scope.rows = 10;
			$scope.cols = 10;
			$scope.options= {
				initialValues: arrList
			};

			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			
			arrList.forEach(function(n){
				var ele = scope.getElementInGrid(tbody, n[0], n[1]);
				expect($(ele).hasClass('dead')).toBeFalsy();
				expect($(ele).hasClass('alive')).toBeTruthy();
			});
			
		});

		it('angular: one alone alive cell should die in nextStep', function(){
			var scope = this;
			var arrList = [
				[3, 2]
			];

			var $scope = this.$rootScope.$new();
			$scope.rows = 10;
			$scope.cols = 10;
			$scope.options = {
				initialValues: arrList
			};

			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			this.$rootScope.$broadcast('next');
			this.$rootScope.$digest();

			ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});

		it('angular: two alone alive should die in nextStep', function(){
			var scope = this;
			var arrList = [
				[3, 2],[3, 3]
			];

			var $scope = this.$rootScope.$new();
			$scope.rows = 5;
			$scope.cols = 5;
			$scope.options = {
				initialValues: arrList
			};

			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			ele = scope.getElementInGrid(tbody, 3, 3);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			this.$rootScope.$broadcast('next');
			this.$rootScope.$digest();

			ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();

			ele = scope.getElementInGrid(tbody, 3, 3);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});


		it('angular: three alive cell should make dead one alive', function(){
			var scope = this;
			var arrList = [
				[2, 5],
				[3, 5],[3,6]
			];

			var $scope = this.$rootScope.$new();
			$scope.rows = 10;
			$scope.cols = 20;
			$scope.options = {
				initialValues: arrList
			};
			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 2, 6);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();

			this.$rootScope.$broadcast('next');
			this.$rootScope.$digest();

			ele = scope.getElementInGrid(tbody, 2, 6);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

		});

		it('angular: more then three alive cell should kill', function(){
			var scope = this;
			var arrList = [
				[2, 6],
				[3, 5],[3, 6],[3, 7],
				[4, 6]
			];

			var $scope = this.$rootScope.$new();
			$scope.rows = 15;
			$scope.cols = 15;
			$scope.options = {
				initialValues: arrList
			};
			var element = this.compileDirective(this.template, $scope);

			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 6);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			this.$rootScope.$broadcast('next');
			this.$rootScope.$digest();

			ele = scope.getElementInGrid(tbody, 3, 6);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});


	});
});