describe('Jquery wrapper', function(){

	beforeAll(function(){
		this.getElementInGrid = function(grid, row, col){
			return grid.children[row].children[col];
		};
	});

	describe("#GameOfLife", function(){

		beforeEach(function(){
			this.testContainer = document.createElement('div');
			document.body.appendChild(this.testContainer);
		});

		afterEach(function(){
			document.body.removeChild(this.testContainer);
		});


		it('jquery: should generate grid of 10 X 10', function(){
			$(this.testContainer).gameOfLife(10, 10);
			var tbody = $(this.testContainer).find('tbody')[0];
			expect(tbody.children.length).toEqual(10);
			$.each(tbody.children, function(index, row){
				expect(row.children.length).toEqual(10);
			});
		});

		it('jquery: should not have any alive cell by default', function(){
			$(this.testContainer).gameOfLife(10, 10);
			var tbody = $(this.testContainer).find('tbody')[0];
			$.each(tbody.children, function(rindex, row){
				$.each(row.children, function(cindex, col){
					expect($(col).hasClass('dead')).toBeTruthy();
					expect($(col).hasClass('alive')).toBeFalsy();
				});
			});
		});

		it('jquery: should set alive to initialValues list', function(){
			var scope = this;
			var arrList = [
				[0, 1],[5, 4],
				[3, 4],[4, 9],
				[5, 6],[6, 6]
			];

			$(this.testContainer).gameOfLife(10, 10, {
				initialValues: arrList
			});
			var tbody = $(this.testContainer).find('tbody')[0];
			
			arrList.forEach(function(n){
				var ele = scope.getElementInGrid(tbody, n[0], n[1]);
				expect($(ele).hasClass('dead')).toBeFalsy();
				expect($(ele).hasClass('alive')).toBeTruthy();
			});
			
		});

		it('jquery: one alone alive cell should die in nextStep', function(){
			var scope = this;
			var arrList = [
				[3, 2]
			];

			var instance  = $(this.testContainer).gameOfLife(10, 10, {
				initialValues: arrList
			});
			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			instance.nextStep();

			ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});

		it('jquery: two alone alive should die in nextStep', function(){
			var scope = this;
			var arrList = [
				[3, 2],[3, 3]
			];

			var instance  = $(this.testContainer).gameOfLife(11, 20, {
				initialValues: arrList
			});
			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			ele = scope.getElementInGrid(tbody, 3, 3);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			instance.nextStep();

			ele = scope.getElementInGrid(tbody, 3, 2);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();

			ele = scope.getElementInGrid(tbody, 3, 3);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});


		it('jquery: three alive cell should make dead one alive', function(){
			var scope = this;
			var arrList = [
				[2, 5],
				[3, 5],[3,6]
			];

			var instance  = $(this.testContainer).gameOfLife(5, 10, {
				initialValues: arrList
			});
			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 2, 6);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();

			instance.nextStep();

			ele = scope.getElementInGrid(tbody, 2, 6);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

		});

		it('jquery: more then three alive cell should kill', function(){
			var scope = this;
			var arrList = [
				[2, 6],
				[3, 5],[3, 6],[3, 7],
				[4, 6]
			];

			var instance  = $(this.testContainer).gameOfLife(15, 15, {
				initialValues: arrList
			});
			var tbody = $(this.testContainer).find('tbody')[0];
			var ele = scope.getElementInGrid(tbody, 3, 6);
			expect($(ele).hasClass('dead')).toBeFalsy();
			expect($(ele).hasClass('alive')).toBeTruthy();

			instance.nextStep();

			ele = scope.getElementInGrid(tbody, 3, 6);
			expect($(ele).hasClass('dead')).toBeTruthy();
			expect($(ele).hasClass('alive')).toBeFalsy();
		});


	});
});