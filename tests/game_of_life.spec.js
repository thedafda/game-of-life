describe('Core Engine', function(){

	beforeAll(function(){
		this.getListOfAliveCells = function(grid){
			var i = 0, j = 0, arr = [];
			grid.forEach(function(row, rindex){
				row.forEach(function(col, cindex){
					if(col){
						arr.push([rindex, cindex]);
					}
				});
			});
			return arr;
		};
	});

	describe("#GameOfLife", function(){

		it('should generate grid of 10 X 20', function(){
			var gol = new GameOfLife(10, 20);
			expect(gol.grid.length).toEqual(10);
			gol.grid.forEach(function(row){
				expect(row.length).toEqual(20);
			});
		});

		it('should not have any alive cell bu default', function(){
			var gol = new GameOfLife(10, 20);
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(0);
		});

		it('should set alive to initialValues list', function(){
			var arrList = [
				[0, 1],[5, 4],
				[3, 4],[4, 9],
				[5, 6],[6, 6]
			];
			var gol = new GameOfLife(10, 10, {
				initialValues: arrList
			});
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(arrList.length);
			//expect(this.getListOfAliveCells(gol.grid)).toEqual(jasmine.objectContaining(arrList));
		});

		it('one alone alive cell should die in nextStep', function(){
			var arrList = [
				[3, 4]
			];
			var gol = new GameOfLife(10, 10, {
				initialValues: arrList
			});
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(arrList.length);
			expect(this.getListOfAliveCells(gol.grid)).toEqual(arrList);
			gol.getNextFrame();
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(0);
			expect(this.getListOfAliveCells(gol.grid)).toEqual([]);
		});

		it('two alone alive should die in nextStep', function(){
			var arrList = [
				[3, 4],
				[3, 5]
			];
			var gol = new GameOfLife(10, 10, {
				initialValues: arrList
			});
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(arrList.length);
			expect(this.getListOfAliveCells(gol.grid)).toEqual(arrList);
			gol.getNextFrame();
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(0);
			expect(this.getListOfAliveCells(gol.grid)).toEqual([]);
		});


		it('three alive cell should make dead one alive', function(){
			var arrList = [
				[2, 5],
				[3, 5],[3,6]
			];
			var gol = new GameOfLife(10, 10, {
				initialValues: arrList
			});
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(arrList.length);
			expect(this.getListOfAliveCells(gol.grid)).toEqual(arrList);
			gol.getNextFrame();
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(4);
			expect(this.getListOfAliveCells(gol.grid)).toContain([2,5]);
			expect(this.getListOfAliveCells(gol.grid)).toContain([2,6]);
			expect(this.getListOfAliveCells(gol.grid)).toContain([3,5]);
			expect(this.getListOfAliveCells(gol.grid)).toContain([3,6]);
		});

		it('more then three alive cell should kill', function(){
			var arrList = [
				[2, 6],
				[3, 5],[3, 6],[3, 7],
				[4, 6]
			];
			var gol = new GameOfLife(10, 10, {
				initialValues: arrList
			});
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(arrList.length);
			expect(this.getListOfAliveCells(gol.grid)).toEqual(arrList);
			gol.getNextFrame();
			expect(this.getListOfAliveCells(gol.grid).length).toEqual(8);
			expect(this.getListOfAliveCells(gol.grid)).not.toContain([3, 6]);
		});


	});
});