(function(w){

	$.fn.gameOfLife = function(rows, cols, options){
		options = options || {};
		
		var gol = new GameOfLife(rows, cols, options);
		
		var tmplt = '<table><tbody>', i, j, className, position;
		for (i = 0; i < rows; i++) {
			tmplt += '<tr>';
			for (j = 0; j < cols; j++) {
				className = gol.grid[i][j] ? 'alive' : 'dead';
				position = JSON.stringify({ row: i, col: j});
				tmplt += "<th class='" + className + "' data-position='" + position + "'></th>";
			}
			tmplt += '</tr>';
		}
		tmplt += '</tbody></table>';
		$(this).html(tmplt);
		var tbody = this.find('tbody')[0];

		$(this).on("click", "th", function(e){
			var $target = $(e.target);
				pos = $target.data("position");
			if(gol.getCell(pos.row, pos.col)){
				$target.removeClass('alive').addClass('dead');
				gol.setCell(pos.row, pos.col, false);
			} else {
				$target.removeClass('dead').addClass('alive');
				gol.setCell(pos.row, pos.col, true);
			}
		});

		function nextStep(){
			var $cell, frame = gol.getNextFrame(); 
			frame.alive.forEach(function(n){
				$cell = $(tbody.children[n[0]].children[n[1]]);
				$cell.removeClass('dead').addClass('alive');
			});
			frame.dead.forEach(function(n){
				$cell = $(tbody.children[n[0]].children[n[1]]);
				$cell.removeClass('alive').addClass('dead');
			});
		}

		var interval;

		function startInterval(time){
			clearInterval(interval);
			interval = setInterval(function(){
				nextStep();
			}, time || 500);
		}

		function stopInterval(){
			clearInterval(interval);
		}

		return {
			nextStep: nextStep,
			startInterval: startInterval,
			stopInterval: stopInterval
		};
	};
})(window);