// Angular directive

(function(){

	var tmplt = [
		'<table>',
			'<tbody>',
				'<tr ng-repeat="row in grid track by $index">',
					'<th ng-repeat="col in row track by $index" ng-class="{dead: !col, alive: col}" ng-click="toggleCell($parent.$index, $index)"></th>',
				'</tr>',
			'</tbody>',
		'</table>'
	].join('');

	angular
		.module('GameOfLife',[])
		.directive('gameOfLife', function(){
			return {
				resrict: 'E',
				scope: {
					rows: '=',
					cols: '=',
					options: '='
				},
				template: tmplt,
				controller: ['$scope', '$interval',function($scope, $interval){
					var rows = Number($scope.rows) || 10;
					var cols = Number($scope.cols) || 10;

					var gol = new GameOfLife(rows, cols, $scope.options);

					$scope.grid = gol.grid;
					function nextStep(){
							gol.getNextFrame();
							$scope.grid = gol.grid;
					}

					$scope.toggleCell = function(row, col){
						var value = gol.getCell(row, col);
						if(value === true || value === false){
							gol.setCell(row, col, !value);  
						}
					};

					$scope.$on('next', function(){
							nextStep();
					});

					var interval;

					$scope.$on('start', function(e){
						$interval.cancel(interval);
						interval = $interval(function(){
							nextStep();
						}, e.time || 500);
					});

					$scope.$on('stop', function(){
							$interval.cancel(interval);
					});

				}]
			};
		});

})();