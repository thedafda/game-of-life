(function(w){

	function GameOfLife(rows, cols, options) {
		this.rows = Number(rows) || 10;
		this.cols = Number(cols) || 10;
		this.options = options || {};
		this.grid = this.createGrid();
		this.selectedValues = this.options.initialValues || [];
		this.setInitialGrid(this.selectedValues);
	}

	GameOfLife.prototype.createGrid = function () {
		var arr = [], i, j;
		for (i = 0; i < this.rows; i++) {
			arr[i] = [];
			for (j = 0; j < this.cols; j++) {
				arr[i].push(false);
			}
		}
		return arr;
	};

	GameOfLife.prototype.setInitialGrid = function (valueArr) {
		var scope = this;
		(valueArr || []).forEach(function (n) {
			var row = n[0], col = n[1];
			scope.grid[row][col] = true;
		});
	};

	GameOfLife.prototype.isValidCell = function(row, col){
		return row > -1 && row < this.rows && col > -1 && col < this.cols;
	};

	GameOfLife.prototype.setCell = function (row, col, value) {
		if (!this.isValidCell(row, col)) {
			return;
		}
		var i, pos, index;
		this.grid[row][col] = !!value;
		if(this.grid[row][col]){
			this.selectedValues.push([row, col]);	
		} else {
			for(i = 0; i < this.selectedValues.length; i++){
				pos = this.selectedValues[i];
				if(pos.row === row && pos.col === col){
					index = i;
					break;
				}
			}
			this.selectedValues.splice(index, 1);
		}
	};

	GameOfLife.prototype.getCell = function (row, col) {
		if (this.isValidCell(row, col)) {
			return this.grid[row][col];
		}
		return;
	};

	GameOfLife.prototype.getNeighborsArrayIndexs = function (row, col) {
		return [[row - 1, col], [row - 1, col - 1], [row, col - 1], [row + 1, col], [row, col + 1], [row + 1, col + 1], [row + 1, col - 1], [row - 1, col + 1]];
	};

	GameOfLife.prototype.isAlive = function (row, col) {
		return !!(this.grid[row] && this.grid[row][col]);
	};

	GameOfLife.prototype.numberOfNeighbors = function (row, col) {
		var scope = this;
		var arrIndexs = scope.getNeighborsArrayIndexs(row, col);
		var neighbors = 0;
		arrIndexs.forEach(function(n){
			if(scope.isAlive(n[0],n[1])){
				neighbors += 1;
			}
		});
		return neighbors;
	};

	GameOfLife.prototype.nextFrameCandidate = function () {
		var scope = this, allPeopleAffected = _.clone(scope.selectedValues);

		scope.selectedValues.forEach(function (n) {
			scope.getNeighborsArrayIndexs(n[0], n[1]).forEach(function (arr) {
				var row = arr[0], col = arr[1];
				if (scope.isValidCell(row, col)) {
					allPeopleAffected.push([row, col]);
				}
			});
		});

		return _.uniq(allPeopleAffected, function (n) {
			return n.toString();
		});
	};

	GameOfLife.prototype.getNextFrame = function () {
		var scope = this;
		var next = {
			alive: [],
			dead: []
		};
		var candidate = scope.nextFrameCandidate();
		candidate.forEach(function (n) {
			var row = n[0], col = n[1];
			var neighbors = scope.numberOfNeighbors(row, col);
			// Any live cell with fewer than two live neighbours dies, as if caused by under-population.
			// Any live cell with two or three live neighbours lives on to the next generation.
			// Any live cell with more than three live neighbours dies, as if by overcrowding.
			// Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
			var value = false;
			if (scope.grid[row][col]) {
				value = (neighbors === 2 || neighbors === 3) ? true : false;
			} else {
				value = (neighbors === 3) ? true : false;
			}

			if (value) {
				next.alive.push([row, col]);
			} else {
				next.dead.push([row, col]);
			}
		});

		next.alive.forEach(function (n) {
			scope.grid[n[0]][n[1]] = true;
		});

		next.dead.forEach(function (n) {
			scope.grid[n[0]][n[1]] = false;
		});

		scope.selectedValues = next.alive;
		return next;
	};

	w.GameOfLife = GameOfLife;

})(window);