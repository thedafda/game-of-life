var initialGrid = [
	/* squar 3 X 3*/
	[5, 5],[5, 6],[5, 7],
	[6, 5],[6, 6],[6, 7],
	[7, 5],[7, 6],[7, 7],
		
	/* */
	[18, 21],
	[19, 22],
	[20, 20],[20, 21],[20, 22]
];

$(function(){

	// jquery plugin example
	var instance = $('#jq-1').gameOfLife(30, 30, { initialValues: initialGrid });

	$(document).on('click', '[action]', function(e){
		var action = $(e.target).attr('action');
		if(action === 'next') {
			instance.nextStep();
		} else if(action === 'start') {
			instance.startInterval();
		} else if(action === 'stop') {
			instance.stopInterval();
		}
	});

});

(function(){
	// Angular Directive Example
	angular
		.module('app', ['GameOfLife'])
		.controller('mainController', ['$scope',function($scope){
			$scope.rows = 30;
			$scope.cols = 30;
			$scope.options = {
				initialValues: initialGrid
			};

			$scope.next = function(){
				$scope.$broadcast('next');
			};

			$scope.start = function(){
				$scope.$broadcast('start');
			};

			$scope.stop = function(){
				$scope.$broadcast('stop');
			};
		}]);
})();

